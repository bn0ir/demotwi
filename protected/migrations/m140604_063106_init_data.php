<?php

class m140604_063106_init_data extends CDbMigration
{
	/*
	public function up()
	{
	}

	public function down()
	{
		echo "m140604_063106_init_data does not support migration down.\n";
		return false;
	}
	*/

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('users', array(
			'id' => 'pk',
			'email' => 'string NOT NULL',
			'password' => 'string NOT NULL',
			'isadmin' => 'boolean',
		));
		$this->insert('users', array(
			'email' => 'admin@admin.ru',
			'password' => crypt('admin'),
			'isadmin' => 1,
		));
		$this->createTable('twits', array(
			'id' => 'pk',
			'message' => 'string NOT NULL',
			'cdate' => 'datetime NOT NULL',
			'user_id' => 'integer NOT NULL REFERENCES users(id)',
		));
	}

	public function safeDown()
	{
		$this->dropTable('twits');
		$this->dropTable('users');
	}
}
