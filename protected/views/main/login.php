<div class="container logincont">
	<ul class="nav nav-tabs">
		<li class="<?php if ($action=='login'): ?>active<?php endif; ?>"><a href="#login" data-toggle="tab">Login</a></li>
		<li class="<?php if ($action=='register'): ?>active<?php endif; ?>"><a href="#register" data-toggle="tab">Register</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane <?php if ($action=='login'): ?>active<?php endif; ?>" id="login">
			<form role="form" action="/login/" method="post" name="loginform">
				<?php if ($model->getErrors()): ?>
					<div class="alert alert-danger">Wrong email or password</div>
				<?php endif; ?>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="form-control" id="email" name="email" placeholder="Enter email" value="<?php echo $model->email; ?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-primary">Login</button>
			</form>
		</div>
		<div class="tab-pane <?php if ($action=='register'): ?>active<?php endif; ?>" id="register">
			<form role="form" action="/register/" method="post" name="registerform">
				<?php if ($model->getErrors()): ?>
					<div class="alert alert-danger">Wrong email or password</div>
				<?php endif; ?>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="form-control" id="email" name="email" placeholder="Enter email" value="<?php echo $model->email; ?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Password">
				</div>
				<button type="submit" class="btn btn-primary">Register</button>
			</form>
		</div>
	</div>
</div>
