<div class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="/">DemoTwi</a>
		</div>
		<a class="btn btn-danger navbar-right logoutbtn" href="/logout/">Logout</a>
		<a class="btn btn-primary navbar-right addbtn" href="#">Add message</a>
	</div>
</div>
<div class="container maincontainer">
<?php foreach ($recent as $item): ?>
<div class="mainitem well">
	<?php if ($item->canEdit($me)): ?>
	<div class="text-right">
		<span class="glyphicon glyphicon-pencil modactions" data-id="<?php echo $item->id; ?>" data-action="edit"></span>
		<span class="glyphicon glyphicon-remove modactions" data-id="<?php echo $item->id; ?>" data-action="delete"></span>
	</div>
	<?php endif; ?>
	<p><?php echo $item->message; ?></p>
</div>
<?php endforeach; ?>
</div>
