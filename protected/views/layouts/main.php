<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title></title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
	<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="/css/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/bootbox.min.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
</head>
<body id="body">
<?php echo $content; ?>
</body>
</html>
