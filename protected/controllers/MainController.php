<?php

class MainController extends Controller
{

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function actionIndex()
	{
		$recent = Twit::model()->with(array('user'))->findAll(array(
			'order' => 'cdate DESC'
		));
		$me = User::model()->findByPk(Yii::app()->user->id);
		$this->render('index', array(
			'recent' => $recent,
			'me' => $me
		));
	}

	public function actionLogin()
	{
		$model = new LoginForm('login');

		if (isset($_POST['email']) && isset($_POST['password']))
		{
			$model->attributes = array(
				'email' => $_POST['email'],
				'password' => $_POST['password'],
			);
			if($model->validate() && $model->login()){
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		$this->render('login',array(
			'model'=>$model,
			'action'=>'login'
		));
	}

	public function actionRegister()
	{
		$model = new LoginForm('register');

		if (isset($_POST['email']) && isset($_POST['password']))
		{
			$model->attributes = array(
				'email' => $_POST['email'],
				'password' => $_POST['password'],
			);
			if($model->validate() && $model->register()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		$this->render('login',array(
			'model'=>$model,
			'action'=>'register'
		));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('login', 'logout', 'register'),
				'users'=>array('*'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);

	}
}
