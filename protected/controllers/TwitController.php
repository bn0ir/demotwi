<?php

class TwitController extends Controller
{

	protected function renderJSON($data)
	{
		header('Content-type: application/json');
		echo CJSON::encode($data);
		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false; // disable any weblogroutes
			}
		}
		Yii::app()->end();
	}

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function actionCreate()
	{
		$error = '';
		$model = new TwitForm('create');
		if (isset($_POST['message'])){
			$model->attributes = array(
				'message' => filter_var($_POST['message'], FILTER_SANITIZE_STRING),
			);
			if($model->validate() && $model->create()){
				$this->renderJSON(array(
					'message'=>$model->message,
					'type'=>'ok'
				));
				return true;
			}
			else {
				$error = 'Your message too long';
			}
		}
		else {
			$error = 'Wrong message';
		}
		$this->renderJSON(array(
			'message'=>$error,
			'type'=>'error'
		));
	}

	public function actionEdit()
	{
		$error = '';
		$type = 'error';
		$model = new TwitForm('edit');
		if (isset($_POST['message']) && isset($_POST['id'])){
			$model->attributes = array(
				'id' => intval($_POST['id']),
				'message' => filter_var($_POST['message'], FILTER_SANITIZE_STRING),
			);
			if($model->validate() && $model->update()){
				$type = 'ok';
			}
			else {
				$error = 'Wrong message id or message too long';
			}
		}
		else {
			$error = 'Wrong message id or message';
		}
		$this->renderJSON(array(
			'message'=>$error,
			'type'=>$type
		));
	}

	public function actionDelete()
	{
		$error = '';
		$type = 'error';
		$model = new TwitForm('delete');
		if (isset($_POST['id'])){
			$model->attributes = array(
				'id' => intval($_POST['id'])
			);
			if ($model->validate() && $model->delete()){
				$type = 'ok';
			}
			else {
				$error = 'You have no permissions to edit this message';
			}
		}
		else {
			$error = 'Wrong message id';
		}
		$this->renderJSON(array(
			'message'=>$error,
			'type'=>$type
		));
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('create', 'edit', 'delete'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);

	}
}
