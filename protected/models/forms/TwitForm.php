<?php

class TwitForm extends CFormModel
{
	public $message;
	public $id;
	private $_twit;

	public function rules()
	{
		return array(
			array('message', 'required', 'on' => array('create', 'edit')),
			array('message', 'lenCheck', 'on' => array('create', 'edit')),
			array('id', 'required', 'on' => array('edit', 'delete')),
			array('id', 'exists', 'on' => array('edit', 'delete')),
		);
	}

	public function lenCheck()
	{
		if(!$this->hasErrors())
		{
			if (mb_strlen($this->message)>200){
				$this->addError('message','Your message is too long!');
			}
		}
	}

	public function exists($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$twit = Twit::model()->findByPk(intval($this->id));
			if (!$twit){
				$this->addError('message','Twit does not exist');
			}
			else {
				$this->_twit = $twit;
			}
		}
	}

	public function create()
	{
		$now = new DateTime();
		$record = new Twit();
		$record->message = $this->message;
		$record->cdate = $now->format('Y-m-d H:i:s');
		$record->user_id = Yii::app()->user->id;
		$record->save();
		BEmail::sendEmailToAdmin('Message created!', "Message text:\r\n".$this->message);
		return true;
	}

	public function update()
	{
		$this->_twit->message = $this->message;
		$this->_twit->save();
		return true;
	}

	public function delete()
	{
		$me = User::model()->findByPk(Yii::app()->user->id);
		if ($this->_twit && $this->_twit->canEdit($me)){
			$this->_twit->delete();
			return true;
		}
		return false;
	}
}
