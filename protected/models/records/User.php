<?php
class User extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'users';
	}

	public function relations()
	{
		return array(
			'twits' => array(self::HAS_MANY, 'Twit', 'user_id'),
		);
	}

}
