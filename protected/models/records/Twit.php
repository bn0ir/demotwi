<?php
class Twit extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'twits';
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function canEdit($user)
	{
		if ($user->id == $this->user->id || $user->isadmin){
			return true;
		}
		else {
			return false;
		}
	}
}
