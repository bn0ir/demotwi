<?php

class BEmail extends CComponent
{

	public static function sendEmail($from, $to, $subject, $message)
	{
		//need to be rewrite using async query (celery)
		$to = $to;
		$subject = $subject;
		$message = $message;
		$headers = "From: ".$from."\r\n" ."Reply-To: ".$from."\r\n".'X-Mailer: PHP/'.phpversion();
		mail($to, $subject, $message, $headers);
	}

	public static function sendEmailToAdmin($subject, $message){
		$from = 'server@demotwi';
		$tos = User::model()->findByAttributes(array(
			'isadmin'=>1
		));
		foreach ($tos as $to){
			self::sendEmail($from, $to, $subject, $message);
		}
	}

}
