<?php

class UserIdentity extends CUserIdentity
{
	
	private $_id;	

	public function authenticate()
	{
		$record = User::model()->findByAttributes(array('email'=>$this->username));

		if ($record===null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		else if ($record->password!==crypt($this->password,$record->password)) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}
		else
        {
			$this->_id = $record->id;
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function register()
	{
		$validator = new CEmailValidator;
		if ($validator->validateValue($this->username) && !$this->checkUserExists())
		{
			$record = new User();
    		$record->email = $this->username;
			$record->password = crypt($this->password);
			$record->isadmin = 0;
			$record->save();
			BEmail::sendEmailToAdmin('User registered!', "Username: ".$this->username);
			$this->_id = $record->id;
			$this->errorCode = self::ERROR_NONE;
		}
		else {
    		$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		return !$this->errorCode;
	}

	private function checkUserExists(){
		$record = User::model()->findByAttributes(array('email'=>$this->username));
		if ($record===null) {
			return false;
		}
		return true;
	}

	public function getId()
    {
        return $this->_id;
    }

}
