$(document).ready(function () {

	$('.logincont .nav-tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('.addbtn').click(function (e){
		e.preventDefault();
		bootbox.prompt("Enter a message", function(result) {                
			if (result === null) {                                                                           
			} else {
				$.post('/twit/create/', {'message':result}, function(data){
					if (data.type == 'error'){
						bootbox.alert(data.message, function() {});
					}
					else {
						window.location = "/";
					}
				});
			}
		});
		setBootboxLenCheck()
	});

	$('.modactions').click(function (e) {
		var messageid = $(e.target).data('id');
		var messageaction = $(e.target).data('action');
		var messagetext = $(e.target).parent().parent().find('p').html();
		if (messageaction == 'edit'){
			bootbox.prompt("Edit the message", function(result) {
				if (result === null) {                                                                           
				} else {
					$.post('/twit/edit/', {'message':result, 'id': messageid}, function(data){
						if (data.type == 'error'){
							bootbox.alert(data.message, function() {});
						}
						else {
							window.location = "/";
						}
					});
				}
			});
			$(".bootbox-input-text").val(messagetext);
			setBootboxLenCheck();
		}
		else if (messageaction == 'delete'){
			bootbox.confirm("Are you sure?", function(result) {
				if (result){
					$.post('/twit/delete/', {'id':messageid}, function (data){
						if (data.type == 'error'){
							bootbox.alert(data.message, function() {});
						}
						else {
							window.location = "/";
						}
					});
				}
			});
		}
	});
})

function setBootboxLenCheck(){
	$(".bootbox-input-text").keyup(function (e){
		if ($(".bootbox-input-text").val().length>200){
			$(".bootbox-input-text").css({'color':'red'});
		}
		else {
			$(".bootbox-input-text").css({'color':'black'});
		}
	});
}
